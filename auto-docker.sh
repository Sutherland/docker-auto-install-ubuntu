#!/usr/bin/env bash

#UBUNTU VERSION ARRAYS
VERSIONARRAY=(18.04 18.04.1 18.04.2 19.04 19.10)
EIGHTEENARRAY=(18.04 18.04.1 18.04.2)
NINETEENARRAY=(19.04 19.10)

#CHECKING UBUNTU VERSION
echo -e "######## \e[32mCHECKING UBUNTU VERSION\e[39m ########"
VERSION=$(lsb_release -rs)

if [[ ! " ${VERSIONARRAY[@]} " =~ " ${VERSION} " ]]; then
    echo -e ""
    echo -e "This script does nopt support your OS/Distribution."
    echo -e "Goodbye!"
    echo -e ""
    exit 1
fi

echo -e ""
echo -e "Found Ubuntu Version: $VERSION"

#INSTALLING UPDATES
sudo apt-get update -y -qq > /dev/null
echo -e ""

#INSTALLING PREPEQUISITES
echo -e "######## \e[32mINSTALLING PREPEQUISITES\e[39m ########"
sudo apt install -y apt-transport-https ca-certificates curl software-properties-common > /dev/null

#ADDING REPOSITORY
echo -e "######## \e[32mADDING REPOISTORY\e[39m ########"
echo -e ""
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
echo -e ""
if [[ " ${EIGHTEENARRAY[@]} " =~ " ${VERSION} " ]]; then
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
fi
if [[ " ${NINETEENARRAY[@]} " =~ " ${VERSION} " ]]; then
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu disco stable"
fi
echo -e ""

#INSTALLING DOCKER
echo -e "######## \e[32mINSTALLING DOCKER\e[39m ########"
sudo apt install -y docker-ce > /dev/null

read -p "Do you want to run Docker without sudo command? (y/n)" -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    sudo usermod -aG docker ${USER}
fi
echo -e ""

#INSTALLATION FINISHED
echo -e "######## \e[32mINSTALLATION FINISHED\e[39m ########"
echo -e ""