## I'm Still Alive!. 
Been away for a while, had a baby! (Well I didn't) sorry for those that have been visiting and following the status of the next script release. 
## The Script.
The script installs Docker Community Edition on Ubuntu versions 18.04 - 19.10
## Get the Script.
```
cd ~
git clone https://gitlab.com/Sutherland/docker-auto-install-ubuntu.git
```
## Run the Script.
```
cd ~/docker-auto-install-ubuntu
sudo bash ./auto-docker.sh
```
During the installation you will be asked if you would like the docker command to be run without the need for sudo, simply enter y or n.
If you read this and you would like to give any feedback or discuss the script any further, please email sutherland@scripting.online.
